from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, TemplateView
from .models import Post, Category, Comment, Preset
from .forms import PostForm, EditForm, CommentForm, PresetForm
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect

class HomeView(TemplateView):
    template_name = 'home.html'

class PresetsView(ListView):
    model = Preset
    template_name = "presets.html"

class BlogView(ListView):
    model = Post
    template_name = 'blog.html'
    #ordering = ['-id']
    ordering = ['-post_date']

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(BlogView, self).get_context_data(*args, **kwargs)
        context["cat_menu"] = cat_menu
        return context

class PostDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'

    def get_context_data(self, *args, **kwargs):
        cat_menu = Category.objects.all()
        context = super(PostDetailView, self).get_context_data(*args, **kwargs)

        asign_post = get_object_or_404(Post, id=self.kwargs['pk'])
        total_likes = asign_post.total_likes()
        liked = False

        if asign_post.likes.filter(id=self.request.user.id).exists():
            liked = True

        context["cat_menu"] = cat_menu
        context["total_likes"] = total_likes
        context["liked"] = liked
        return context

class AddPostView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'add_post.html'
    #fields = '__all__'
    #fields = ('title', 'author', 'body')

class AddCategoryView(CreateView):
    model = Category
    template_name = 'add_category.html'
    fields = '__all__'

class AddPresetView(CreateView):
    model = Preset
    form_class = PresetForm
    template_name = 'add_preset.html'

class DeleteCategoryView(DeleteView):
    model = Category
    template_name = 'delete_category.html'
    success_url = reverse_lazy('blog')

class DeletePresetView(DeleteView):
    model = Preset
    template_name = 'delete_preset.html'
    success_url = reverse_lazy('presets')

class EditPostView(UpdateView):
    model = Post
    form_class = EditForm
    #fields = ('title', 'body')
    template_name = 'edit_post.html'

class DeletePostView(DeleteView):
    model = Post
    template_name = 'delete_post.html'
    success_url = reverse_lazy('blog')

class AddCommentView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'add_comment.html'
    success_url = reverse_lazy('blog')

    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)

def CategoryView(request, cats):
    category_posts = Post.objects.filter(category=cats.replace('-', ' ').lower())
    return render(request, 'categories.html', {'cats':cats.title().replace('-', ' '), 'category_posts':category_posts})

def LikeView(request, pk):
    post = get_object_or_404(Post, id=request.POST.get('post_id'))
    liked = False

    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        liked = False
    else:
        post.likes.add(request.user)
        liked = True

    return HttpResponseRedirect(reverse('post_detail', args=[str(pk)]))