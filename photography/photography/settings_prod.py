from .settings import *
import dj_database_url
import django_heroku

DATABASES['default'] = dj_database_url.config(conn_max_age=600, ssl_require=True)

# Activate Django-Heroku.
django_heroku.settings(locals())
