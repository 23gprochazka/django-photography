from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime, date
from ckeditor.fields import RichTextField

class Category(models.Model):
    name = models.CharField(max_length=60)

    def save(self):
        self.name = self.name.lower()
        return super(Category, self).save()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('blog')

class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    bio = models.TextField()
    profile_pic = models.ImageField(null=True, blank=True, upload_to="profile_pictures/")
    website_url = models.CharField(max_length=255, blank=True, null=True)
    instagram_url = models.CharField(max_length=255, blank=True, null=True)
    facebook_url = models.CharField(max_length=255, blank=True, null=True)
    twitter_url = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.user)

    def get_absolute_url(self):
        return reverse('home')

class Post(models.Model):
    title = models.CharField(max_length=60)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.CharField(max_length=60)
    #body = models.TextField()
    introduction = models.CharField(max_length=2000)
    body = RichTextField(blank=True, null=True)
    image = models.ImageField(null=True, blank=True, upload_to="blog_images/")
    post_date = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name='blog_post', blank=True)

    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return self.title + ' | ' + str(self.author)

    def get_absolute_url(self):
        return reverse('blog')

class Comment(models.Model):
    post = models.ForeignKey(Post, related_name="comments", on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    body = models.TextField()
    post_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s - %s' % (self.post.title, self.name)

class Preset(models.Model):
    title = models.CharField(max_length=100, null=True)
    download = models.FileField(upload_to='presets/', null=True)
    before = models.ImageField(upload_to='presets/', null=True)
    after = models.ImageField(upload_to='presets/', null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('presets')
