from django import forms
from .models import Post, Category, Comment, Preset

choices = Category.objects.all().values_list('name', 'name')
choice_list = []

for item in choices:
    choice_list.append(item)

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'author', 'category', 'introduction', 'body', 'image')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Post title'}),
            #'author': forms.Select(attrs={'class': 'form-control'}),
            'author': forms.TextInput(attrs={'class': 'form-control', 'id': 'current_user', 'value': '', 'type': 'hidden'}),
            'category': forms.Select(choices=choice_list, attrs={'class': 'form-control'}),
            'introduction': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Introduction to your post'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Post content'}),    
        }

class EditForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'introduction', 'body', 'image')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Post title'}),
            'introduction': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Introduction to your post'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Post content'}),
        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('name', 'body')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'id': 'comment_user', 'value': '', 'type': 'hidden'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Comment content'}),
        }

class PresetForm(forms.ModelForm):
    class Meta:
        model = Preset
        fields = ('title', 'download', 'before', 'after')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Preset name'}),   
        }