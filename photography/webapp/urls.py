from django.urls import path
from .views import BlogView, PostDetailView, AddPostView, EditPostView, DeletePostView, AddCategoryView, CategoryView, LikeView, AddCommentView, HomeView, DeleteCategoryView, PresetsView, AddPresetView, DeletePresetView

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('presets', PresetsView.as_view(), name="presets"),
    path('blog', BlogView.as_view(), name="blog"),
    path('post/<int:pk>', PostDetailView.as_view(), name="post_detail"),
    path('add-post', AddPostView.as_view(), name="add_post"),
    path('add-category', AddCategoryView.as_view(), name="add_category"),
    path('add-preset', AddPresetView.as_view(), name="add_preset"),
    path('edit-post/<int:pk>', EditPostView.as_view(), name="edit_post"),
    path('delete-post/<int:pk>', DeletePostView.as_view(), name="delete_post"),
    path('delete-category/<int:pk>', DeleteCategoryView.as_view(), name="delete_category"),
    path('delete-preset/<int:pk>', DeletePresetView.as_view(), name="delete_preset"),
    path('category/<str:cats>', CategoryView, name="category"),
    path('like/<int:pk>', LikeView, name="like_post"),
    path('post/<int:pk>/add-comment', AddCommentView.as_view(), name="add_comment"),
]